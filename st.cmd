require mrfioc2
require e3-common 

epicsEnvSet("TOP", "$(E3_CMD_TOP)/../..")

epicsEnvSet("IOC", "$(IOC_NAME)")
epicsEnvSet("DEV", "EVG")

iocshLoad("$(mrfioc2_DIR)/env-init.iocsh")

iocshLoad("$(mrfioc2_DIR)/evm-mtca-init.iocsh", "IOC=$(IOC), DEV=$(DEV), PCIID=$(PCI_SLOT)")

iocInit()
